def merge_options(optsfile, extraopts):
   with open('the_opts.py', 'w') as o, open(optsfile, 'r') as f:
      for line in f:
         o.write(line)
      o.write(extraopts)

jobsDicts = [
    # 2016
    {
    "name" : "B02Kpiee-MC-2016-6500GeV-MagDown-Sim09g-Reco16-Stripping28r2-NoPID-11124037",
    "inputdata" : "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/11124037/ALLSTREAMS.DST", 
    "isSimulation" : "True",
    "year" : "2016",
    "DDDB" : "dddb-20170721-3", 
    "CondDB" : "sim-20170721-2-vc-md100"
    },
    {
    "name" : "B02Kpiee-MC-2016-6500GeV-MagUp-Sim09g-Reco16-Stripping28r2-NoPID-11124037",
    "inputdata" : "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09g/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/11124037/ALLSTREAMS.DST", 
    "isSimulation" : "True",
    "year" : "2016",
    "DDDB" : "dddb-20170721-3", 
    "CondDB" : "sim-20170721-2-vc-mu100"
    },
    # 2017
    #{
    #"name" : "B02Kpiee-MC-2012-6500GeV-MagDown-Sim09g-Reco17-Stripping29r2p1-NoPID-11124037",
    #"inputdata" : "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/11124037/ALLSTREAMS.DST",
    #"isSimulation" : "True",
    #"year" : "2017",
    #"DDDB" : "dddb-20170721-3", 
    #"CondDB" : "sim-20190430-1-vc-md100"
    #},
    #{
    #"name" : "B02Kpiee-MC-2012-6500GeV-MagUp-Sim09g-Reco17-Stripping29r2p1-NoPID-11124037",
    #"inputdata" : "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09g/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/11124037/ALLSTREAMS.DST",
    #"isSimulation" : "True",
    #"year" : "2017",
    #"DDDB" : "dddb-20170721-3", 
    #"CondDB" : "sim-20190430-1-vc-mu100"
    #},

]

for jobsDict in jobsDicts:

    #j = Job(name=jobsDict["name"])
    #myApp = prepareGaudiExec('DaVinci','v42r9p2', myPath='.')
    #j.application = myApp

    myApp = GaudiExec()
    myApp.directory = "./DaVinciDev_v44r10p5" 
    j = Job(application=myApp)
    j.application.platform = 'x86_64-centos7-gcc62-opt'
    j.name = jobsDict["name"]

    limit_input_files = -1
    #limit_input_files = 1
    ds = BKQuery(jobsDict["inputdata"], dqflag=['OK']).getDataset()

    if limit_input_files > 0:
      ds_reduced = LHCbDataset()
      n = 0
      for fn in ds.getFileNames():
        if n >= limit_input_files: continue
        ds_reduced.extend( [ 'LFN:'+fn, ] )
        n = n + 1
      ds = ds_reduced  

    j.inputdata     = ds

    if jobsDict["isSimulation"] == "False":
        extraopts = """
from Configurables import CondDB
DaVinci().DataType = '{year}'
DaVinci().TupleFile  = '{name}.root'
DaVinci().Simulation = {isSimulation}
CondDB().UseLatestTags = ['{year}']
DaVinci().DDDBtag = '{DDDB}'
execute_option_file('{inputdata}')
        """.format(**jobsDict)

    #Simulation
    if jobsDict["isSimulation"] == "True":
        extraopts = """
DaVinci().DataType = '{year}'
DaVinci().TupleFile  = '{name}.root'
DaVinci().Simulation = {isSimulation}
DaVinci().CondDBtag = '{CondDB}'
DaVinci().DDDBtag = '{DDDB}'
execute_option_file('{inputdata}')
        """.format(**jobsDict)

    merge_options("options_B02KpiEE_RunII.py", extraopts)
    j.application.options = ["the_opts.py"]

    if jobsDict["isSimulation"] == "False" :
        j.splitter = SplitByFiles(filesPerJob = 5)
    else : 
        j.splitter = SplitByFiles(filesPerJob = 10)
        #j.splitter = SplitByFiles(filesPerJob = 1)

    j.outputfiles = [DiracFile(jobsDict["name"]+".root")]

    j.inputfiles = [ "/home/hep/rsilvaco/Analysis/RareDecays/Bu2K1EE/Repository/ewp-rkpipi/weightfiles/weightsHard.xml", 
                     "/home/hep/rsilvaco/Analysis/RareDecays/Bu2K1EE/Repository/ewp-rkpipi/weightfiles/weightsSoft.xml" ]

    j.comment = jobsDict["name"]

    j.backend = Dirac()

    j.submit()
