from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import DecayTreeTuple, TupleToolTISTOS, TupleToolRecoStats, TupleToolTrigger, TupleToolDira, TupleToolDecay, TupleToolL0Calo

# TrackScaleState is only picked up by DTF variables
from Configurables import TrackScaleState, TrackSmearState
from Configurables import EventNodeKiller
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import StrippingReport
from Configurables import ProcStatusCheck

tuplename = {
    "Kpimumu": "B02Kpimumu",
}

decaydescriptor = {
    "Kpimumu": "[B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(K*_0(1430)0 -> ^K+ ^pi-)]CC", 
}

branches = {
    "Kpimumu": {"B"     : "[^(B0 -> (J/psi(1S) -> mu+ mu-) (K*_0(1430)0 -> K+ pi-))]CC", 
            "K"     : "[ B0 -> (J/psi(1S) -> mu+ mu-) (K*_0(1430)0 -> ^K+ pi-)]CC",
            "Pi"    : "[ B0 -> (J/psi(1S) -> mu+ mu-) (K*_0(1430)0 -> K+ ^pi-)]CC",
            "L1"    : "[ B0 -> (J/psi(1S) -> ^mu+ mu-) (K*_0(1430)0 -> K+ pi-)]CC",
            "L2"    : "[ B0 -> (J/psi(1S) -> mu+ ^mu-) (K*_0(1430)0 -> K+ pi-)]CC",
            "Jpsi"  : "[ B0 -> ^(J/psi(1S) -> mu+ mu-) (K*_0(1430)0 -> K+ pi-)]CC",
            "Kstar" : "[ B0 -> (J/psi(1S) -> mu+ mu-) ^(K*_0(1430)0 -> K+ pi-)]CC", }, 
}

sequencer = {
    "Kpimumu": [],
}


from DecayTreeTuple.Configuration import *


def execute_option_file(path):

    # Run over the K1 line
    if DaVinci().Simulation is True:

        # CALO has to be rerun for S21
        from CommonParticlesArchive import CommonParticlesArchiveConf

        # TODO: fix StrippingArchive for RunI
        if '2011' in DaVinci().DataType:
            strip = 'stripping21r1p2'
            importOptions(
                '$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py')
            CommonParticlesArchiveConf().redirect(strip)
            from StrippingArchive.Stripping21r1p2.StrippingRD.StrippingBu2LLK import Bu2LLKConf, default_config

        elif '2012' in DaVinci().DataType:
            strip = 'stripping21r0p2'
            importOptions(
                '$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py')
            CommonParticlesArchiveConf().redirect(strip)
            from StrippingArchive.Stripping21r0p1.StrippingRD.StrippingBu2LLK import Bu2LLKConf, default_config

        elif '2015' in DaVinci().DataType:
            from StrippingArchive.Stripping24r2.StrippingRD.StrippingBu2LLK import Bu2LLKConf, default_config

        elif '2016' in DaVinci().DataType:
            from StrippingArchive.Stripping28r2.StrippingRD.StrippingBu2LLK import Bu2LLKConf, default_config

        elif '2017' in DaVinci().DataType:
            from StrippingArchive.Stripping29r2.StrippingRD.StrippingBu2LLK import Bu2LLKConf, default_config

        elif '2018' in DaVinci().DataType:
            from StrippingArchive.Stripping34r0p1.StrippingRD.StrippingBu2LLK import Bu2LLKConf, default_config

        else:
            raise Exception(" `DaVinci().DataType` not set correctly.")

        custom_stream = StrippingStream('CustomStream')

        myConfig = default_config["CONFIG"]
        myConfig["PIDe"] = -999999

        lb = Bu2LLKConf('Bu2LLK', myConfig)
        for line in lb.lines():
            if line.name() == 'StrippingBu2LLK_eeLine2_extra':
                custom_stream.appendLines([line])
            if line.name() == 'StrippingBu2LLK_mmLine':
                custom_stream.appendLines([line])

        filterBadEvents = ProcStatusCheck()

        sc = StrippingConf(HDRLocation="SomeNonExistingLocation",
                           Streams=[custom_stream],
                           MaxCandidates=2000,
                           AcceptBadEvents=False,
                           BadEventSelection=filterBadEvents)

        DaVinci().appendToMainSequence([sc.sequence()])

        MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

        sr = StrippingReport(Selections=sc.selections())
        DaVinci().appendToMainSequence([sr])


    for key, mode in tuplename.items():
        makeTuple(key, mode, path)


def makeTuple(channel, tree, path):

    simulation_inputstring = "Phys/Bu2LLK_mmLine_extra/Particles"
    data_inputstring = "/Event/Leptonic/Phys/Bu2LLK_mmLine_extra/Particles"

    toollist = [
          "TupleToolGeometry"
        , "TupleToolKinematic" 
        , "TupleToolEventInfo"
        , "TupleToolPropertime"
        , "TupleToolAngles"
        , "TupleToolTrigger"
        , "TupleToolTrackInfo"
        , "TupleToolPrimaries"
        , "TupleToolDira"
        , "TupleToolTrackPosition"
        , "TupleToolRecoStats"
        , "TupleToolBremInfo"
        , "TupleToolANNPID"
    ]

    from Configurables import GaudiSequencer
    sequencer[channel] = GaudiSequencer('Sequence_' + channel)

    # Check whether it is a DST or MDST file
    import os.path
    extension = os.path.splitext(path)[1]
    if extension.lower() == ".dst":
        DaVinci().InputType = 'DST'
    elif extension.lower() == ".mdst":
        DaVinci().InputType = 'MDST'
    else:
        raise Exception(
            "Extension {extension} of {path} does not match .mdst or .dst".format(extension, path))

    # TrackScaleState is only picked up by DTF variables
    from Configurables import TrackScaleState, TrackSmearState
    from Configurables import EventNodeKiller

    # Kill some nodes if micro dst-file
    if DaVinci().InputType == 'MDST':
        from Configurables import EventNodeKiller
        eventNodeKiller = EventNodeKiller('DAQkiller')
        eventNodeKiller.Nodes = ['/Event/DAQ', '/Event/pRec']
        sequencer[channel].Members += [eventNodeKiller]

    if DaVinci().Simulation is False:  # for Data
        scaler = TrackScaleState('Scaler', RootInTES="/Event/Leptonic/")
        sequencer[channel].Members += [scaler]
    else:  # Re-run Stripping and apply TrackSmearState
        Strip_EventNodeKiller = EventNodeKiller('StripKiller')
        Strip_EventNodeKiller.Nodes = ['/Event/AllStreams', '/Event/Strip']
        sequencer[channel].Members += [Strip_EventNodeKiller]

        # Apply the momentum smearing (for MC only)
        # default configuration is perfectly fine
        smear = TrackSmearState('Smear')
        sequencer[channel].Members += [smear]

    ntuple = DecayTreeTuple(tuplename[channel])

    if DaVinci().Simulation is True:  # for MC
        ntuple.Inputs = [simulation_inputstring]
    elif DaVinci().Simulation is False:  # for Tuple
        ntuple.Inputs = [data_inputstring]
    else:
        raise Exception(" `DaVinci().Simulation` not set.")

    ntuple.Decay = decaydescriptor[channel]
    ntuple.addBranches(branches[channel])

    ntuple.TupleName = tree

    ntuple.ToolList = toollist

    sequencer[channel].Members.append(ntuple)

    if DaVinci().Simulation is True:
        from Configurables import BackgroundCategory

        backgroundinfo = ntuple.addTupleTool("TupleToolMCBackgroundInfo") 

        if '2011' in DaVinci().DataType or '2012' in DaVinci().DataType:
            backgroundinfo.addTool(BackgroundCategory('BackgroundCategory'))
            backgroundinfo.BackgroundCategory.SoftPhotonCut = 100000000000.  # Ignores all photons
            MCTruth = ntuple.addTupleTool("TupleToolMCTruth")
            MCTruth.addTupleTool("MCTupleToolHierarchy")

        elif '2015' in DaVinci().DataType:
            MCTruth = ntuple.addTupleTool("TupleToolMCTruth")
            MCTruth.addTupleTool("MCTupleToolHierarchy")

        elif '2016' in DaVinci().DataType:

            from Configurables import MCMatchObjP2MCRelator 
            default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations') 
            rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc] 

            MCTruth = ntuple.addTupleTool("TupleToolMCTruth")
            MCTruth.addTool(MCMatchObjP2MCRelator) 
            MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs 
            MCTruth.addTupleTool("MCTupleToolHierarchy")

    # PID TupleTool
    if(True):
        # PID information for charged particles
        pid = ntuple.addTupleTool("TupleToolPid")
        pid.Verbose = True  # More information like isMuonLoose etc.

    # TISTOS TupleTool
    if(True):
        from Configurables import TupleToolTISTOS

        L0Triggers = ["L0MuonDecision", "L0DiMuonDecision",
                      "L0HadronDecision", "L0ElectronDecision", "L0PhotonDecision"]

        if '2011' in DaVinci().DataType or '2012' in DaVinci().DataType:
            Hlt1Triggers = ["Hlt1TrackAllL0Decision", "Hlt1TrackMuonDecision",
                            "Hlt1TrackPhotonDecision", "Hlt1DiMuonLowMassDecision", "Hlt1DiMuonHighMassDecision"]

            Hlt2Triggers = [
                # muon lines
                "Hlt2SingleMuonDecision", "Hlt2SingleMuonLowPTDecision", "Hlt2SingleMuonHighPTDecision",
                "Hlt2DiMuonDecision",  "Hlt2DiMuonLowMassDecision",
                "Hlt2DiMuonJPsiDecision",  "Hlt2DiMuonJPsiHighPTDecision",  "Hlt2DiMuonPsi2SDecision",
                "Hlt2DiMuonDetachedDecision",  "Hlt2DiMuonDetachedJPsiDecision", "Hlt2DiMuonDetachedHeavyDecision", "Hlt2TriMuonTauDecision",
                # hadron/Topo lines
                "Hlt2B2HHDecision",
                "Hlt2DiMuonBDecision",  "Hlt2DiMuonZDecision",
                "Hlt2TopoMu2BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2TopoMu4BodyBBDTDecision",
                "Hlt2Topo2BodyBBDTDecision",   "Hlt2Topo3BodyBBDTDecision",   "Hlt2Topo4BodyBBDTDecision",
                "Hlt2Topo2BodySimpleDecision", "Hlt2Topo3BodySimpleDecision", "Hlt2Topo4BodySimpleDecision",
                "Hlt2TopoE2BodyBBDTDecision",  "Hlt2TopoE3BodyBBDTDecision",  "Hlt2TopoE4BodyBBDTDecision",
                "Hlt2TopoE2BodySimpleDecision", "Hlt2TopoE3BodySimpleDecision", "Hlt2TopoE4BodySimpleDecision",
                # others
                "Hlt2PassThroughDecision",
                "Hlt2TransparentDecision",
                # inclusive decisions
                "Hlt2DiMuonDY.*Decision", "Hlt2TopoE.*Decision", "Hlt2Topo.*Decision",  "Hlt2Charm.*Decision", "Hlt2DiElectron.*Decision"
            ]

        else: 
            Hlt1Triggers = ["Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision", "Hlt1TrackMVALooseDecision", "Hlt1TwoTrackMVALooseDecision", "Hlt1TrackMuonDecision", "Hlt1TrackMuonMVADecision",
                            "Hlt1DiMuonHighMassDecision", "Hlt1DiMuonLowMassDecision", "Hlt1SingleMuonHighPTDecision", "Hlt1DiMuonNoL0Decision",
                            "Hlt1L0AnyDecision", "Hlt1L0AnyNoSPDDecision", "Hlt1SingleElectronNoIPDecision", "Hlt1SingleMuonNoIPDecision", "Hlt1GlobalDecision"]

            Hlt2Triggers = [
                "Hlt2DiMuonBDecision",
                "Hlt2DiMuonDetachedDecision",
                "Hlt2DiMuonDetachedHeavyDecision",
                "Hlt2DiMuonDetachedJPsiDecision",
                "Hlt2DiMuonDetachedPsi2SDecision",
                "Hlt2DiMuonJPsiDecision",
                "Hlt2DiMuonJPsiHighPTDecision",
                "Hlt2DiMuonPsi2SDecision",
                "Hlt2DiMuonPsi2SHighPTDecision",
                "Hlt2DiMuonSoftDecision",
                "Hlt2DiMuonZDecision",
                "Hlt2LowMultDiMuonDecision",
                "Hlt2LowMultDiMuon_PSDecision",
                "Hlt2LowMultMuonDecision",
                "Hlt2SingleMuonDecision",
                "Hlt2SingleMuonHighPTDecision",
                "Hlt2SingleMuonLowPTDecision",
                "Hlt2SingleMuonRareDecision",
                # hadron/Topo lines
                "Hlt2TopoMu2BodyBBDTDecision",
                "Hlt2TopoMu3BodyBBDTDecision",
                "Hlt2TopoMu4BodyBBDTDecision",
                "Hlt2Topo2BodyBBDTDecision",
                "Hlt2Topo3BodyBBDTDecision",
                "Hlt2Topo4BodyBBDTDecision",
                "Hlt2Topo2BodySimpleDecision",
                "Hlt2Topo3BodySimpleDecision",
                "Hlt2Topo4BodySimpleDecision",
                "Hlt2TopoE2BodyBBDTDecision",
                "Hlt2TopoE3BodyBBDTDecision",
                "Hlt2TopoE4BodyBBDTDecision",
                "Hlt2TopoE2BodySimpleDecision",
                "Hlt2TopoE3BodySimpleDecision",
                "Hlt2TopoE4BodySimpleDecision",
                # Topo
                "Hlt2Topo2BodyDecision",
                "Hlt2Topo3BodyDecision",
                "Hlt2Topo4BodyDecision",
                "Hlt2TopoE2BodyDecision",
                "Hlt2TopoE3BodyDecision",
                "Hlt2TopoE4BodyDecision",
                "Hlt2TopoEE2BodyDecision",
                "Hlt2TopoEE3BodyDecision",
                "Hlt2TopoEE4BodyDecision",
                "Hlt2TopoMu2BodyDecision",
                "Hlt2TopoMu3BodyDecision",
                "Hlt2TopoMu4BodyDecision",
                "Hlt2TopoMuE2BodyDecision",
                "Hlt2TopoMuE3BodyDecision",
                "Hlt2TopoMuE4BodyDecision",
                "Hlt2TopoMuMu2BodyDecision",
                "Hlt2TopoMuMu3BodyDecision",
                "Hlt2TopoMuMu4BodyDecision",
                "Hlt2TopoMuMuDDDecision",
                # inclusive decisions
                "Hlt2DiMuonDY.*Decision", "Hlt2TopoE.*Decision", "Hlt2Topo.*Decision",  "Hlt2Charm.*Decision", "Hlt2DiElectron.*Decision"
            ]

        triggerList = L0Triggers + Hlt1Triggers + Hlt2Triggers

        ntuple.addTupleTool("TupleToolTISTOS")
        ntuple.TupleToolTISTOS.VerboseL0 = True
        ntuple.TupleToolTISTOS.VerboseHlt1 = True
        ntuple.TupleToolTISTOS.VerboseHlt2 = True
        ntuple.TupleToolTISTOS.FillL0 = True
        ntuple.TupleToolTISTOS.FillHlt1 = True
        ntuple.TupleToolTISTOS.FillHlt2 = True
        ntuple.TupleToolTISTOS.OutputLevel = INFO
        ntuple.TupleToolTISTOS.TriggerList = triggerList

    from Configurables import LoKi__Hybrid__TupleTool

    # Setting the name of each branch particle    
    ntuple.addTool(TupleToolDecay, name='B')
    ntuple.addTool(TupleToolDecay, name='Jpsi')
    ntuple.addTool(TupleToolDecay, name='Kstar')
    ntuple.addTool(TupleToolDecay, name='L1')
    ntuple.addTool(TupleToolDecay, name='L2')
    ntuple.addTool(TupleToolDecay, name='K')
    ntuple.addTool(TupleToolDecay, name='Pi')

    # Constrain the B to originate from the primary vertex
    ntuple.addTool(TupleToolDecay, name="B")
    ntuple.B.addTupleTool('TupleToolDecayTreeFitter/DTF_PV')
    ntuple.B.DTF_PV.Verbose = True
    ntuple.B.DTF_PV.constrainToOriginVertex = True

    # Constrain the B to originate from the primary vertex and Jpsi mass
    ntuple.B.addTupleTool('TupleToolDecayTreeFitter/DTF_PVandJpsi')
    ntuple.B.DTF_PVandJpsi.Verbose = True
    ntuple.B.DTF_PVandJpsi.constrainToOriginVertex = True
    ntuple.B.DTF_PVandJpsi.daughtersToConstrain = ['J/psi(1S)']

    # Constrain the B to originate from the primary vertex and psi(2S) mass
    ntuple.B.addTupleTool('TupleToolDecayTreeFitter/DTF_PVandPsi2S')
    ntuple.B.DTF_PVandPsi2S.Verbose = True
    ntuple.B.DTF_PVandPsi2S.constrainToOriginVertex = True
    ntuple.B.DTF_PVandPsi2S.Substitutions = {'[B0 -> ^J/psi(1S) K*_0(1430)0]CC':'psi(2S)'}
    ntuple.B.DTF_PVandPsi2S.daughtersToConstrain = ['psi(2S)']

    # Calorimeter information
    ntuple.K.addTupleTool('TupleToolL0Calo/K_L0Calo')
    ntuple.Pi.addTupleTool('TupleToolL0Calo/Pi_L0Calo')
    ntuple.L1.addTupleTool('TupleToolL0Calo/L1_L0Calo')
    ntuple.L1.L1_L0Calo.WhichCalo = "ECAL" 
    ntuple.L2.addTupleTool('TupleToolL0Calo/L2_L0Calo')
    ntuple.L2.L2_L0Calo.WhichCalo = "ECAL" 
    
    # Substitution ID
    substitutions = [
        'K+ => pi+',
        'K+ => p+',
        'K+ => mu+',
        'pi+ => K+',
        'pi+ => p+',
        'pi+ => mu+',
        'mu+ => K+',
        'mu+ => pi+',
        'mu+ => p+'
    ]
    doubleSubstitutions = [
        'K+/pi- => pi+/K-',
        'K+/pi- => p+/K-',
        'K+/pi- => pi+/p~-',
        'K+/mu+ => mu+/K+',
        'pi+/mu+ => mu+/pi+' 
    ]
    tupleToolBSubMass = ntuple.B.addTupleTool('TupleToolSubMass/B_SubMass')
    tupleToolBSubMass.Substitution += substitutions
    tupleToolBSubMass.DoubleSubstitution += doubleSubstitutions

    # Add RelInfo from Stripping
    LoKi_Extra = LoKi__Hybrid__TupleTool("LoKi_Extra")
    LoKi_Extra.Variables = {
        "AMAXDOCA":  " LoKi.Particles.PFunA( AMAXDOCA ('LoKi::TrgDistanceCalculator') ) ",
        "AMINDOCA":  " LoKi.Particles.PFunA( AMINDOCA ('LoKi::TrgDistanceCalculator') ) ",
        "LOKI_MCORRBEST":  "BPVCORRM",

        "VTXISONUMVTX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoInfo', 'VTXISONUMVTX', -999.)",
        "VTXISODCHI2ONETRACK": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoInfo', 'VTXISODCHI2ONETRACK', -999.)",
        "VTXISODCHI2MASSONETRACK": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoInfo', 'VTXISODCHI2MASSONETRACK', -999.)",
        "VTXISODCHI2TWOTRACK": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoInfo', 'VTXISODCHI2TWOTRACK', -999.)",
        "VTXISODCHI2MASSTWOTRACK": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoInfo', 'VTXISODCHI2MASSTWOTRACK', -999.)",

        "VTXISOBDTHARDFIRSTVALUE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoBDTInfo', 'VTXISOBDTHARDFIRSTVALUE', -999.)",
        "VTXISOBDTHARDSECONDVALUE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoBDTInfo', 'VTXISOBDTHARDSECONDVALUE', -999.)",
        "VTXISOBDTHARDTHIRDVALUE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoBDTInfo', 'VTXISOBDTHARDTHIRDVALUE', -999.)",
        "VTXISOBDTSOFTFIRSTVALUE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoBDTInfo', 'VTXISOBDTSOFTFIRSTVALUE', -999.)",
        "VTXISOBDTSOFTSECONDVALUE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoBDTInfo', 'VTXISOBDTSOFTSECONDVALUE', -999.)",
        "VTXISOBDTSOFTTHIRDVALUE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/VertexIsoBDTInfo', 'VTXISOBDTSOFTSECONDVALUE', -999.)",

        "H1_CONEANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEANGLE', -999.)",
        "H1_CONEMULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEMULT', -999.)",
        "H1_CONEPX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPX', -999.)",
        "H1_CONEPY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPY', -999.)",
        "H1_CONEPZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPZ', -999.)",
        "H1_CONEP": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEP', -999.)",
        "H1_CONEPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPT', -999.)",
        "H1_CONEPXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPXASYM', -999.)",
        "H1_CONEPYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPYASYM', -999.)",
        "H1_CONEPZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPZASYM', -999.)",
        "H1_CONEPASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPASYM', -999.)",
        "H1_CONEPTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEPTASYM', -999.)",
        "H1_CONEDELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEDELTAETA', -999.)",
        "H1_CONEDELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH1', 'CONEDELTAPHI', -999.)",

        "H2_CONEANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEANGLE', -999.)",
        "H2_CONEMULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEMULT', -999.)",
        "H2_CONEPX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPX', -999.)",
        "H2_CONEPY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPY', -999.)",
        "H2_CONEPZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPZ', -999.)",
        "H2_CONEP": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEP', -999.)",
        "H2_CONEPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPT', -999.)",
        "H2_CONEPXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPXASYM', -999.)",
        "H2_CONEPYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPYASYM', -999.)",
        "H2_CONEPZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPZASYM', -999.)",
        "H2_CONEPASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPASYM', -999.)",
        "H2_CONEPTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEPTASYM', -999.)",
        "H2_CONEDELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEDELTAETA', -999.)",
        "H2_CONEDELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoH2', 'CONEDELTAPHI', -999.)",

        "L1_CONEANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEANGLE', -999.)",
        "L1_CONEMULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEMULT', -999.)",
        "L1_CONEPX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPX', -999.)",
        "L1_CONEPY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPY', -999.)",
        "L1_CONEPZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPZ', -999.)",
        "L1_CONEP": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEP', -999.)",
        "L1_CONEPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPT', -999.)",
        "L1_CONEPXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPXASYM', -999.)",
        "L1_CONEPYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPYASYM', -999.)",
        "L1_CONEPZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPZASYM', -999.)",
        "L1_CONEPASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPASYM', -999.)",
        "L1_CONEPTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEPTASYM', -999.)",
        "L1_CONEDELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEDELTAETA', -999.)",
        "L1_CONEDELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL1', 'CONEDELTAPHI', -999.)",

        "L2_CONEANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEANGLE', -999.)",
        "L2_CONEMULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEMULT', -999.)",
        "L2_CONEPX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPX', -999.)",
        "L2_CONEPY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPY', -999.)",
        "L2_CONEPZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPZ', -999.)",
        "L2_CONEP": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEP', -999.)",
        "L2_CONEPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPT', -999.)",
        "L2_CONEPXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPXASYM', -999.)",
        "L2_CONEPYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPYASYM', -999.)",
        "L2_CONEPZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPZASYM', -999.)",
        "L2_CONEPASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPASYM', -999.)",
        "L2_CONEPTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEPTASYM', -999.)",
        "L2_CONEDELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEDELTAETA', -999.)",
        "L2_CONEDELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoInfoL2', 'CONEDELTAPHI', -999.)",

        "H1_BSMUMUTRACKPLUSISO": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH1', 'BSMUMUTRACKPLUSISO', -999.)",
        "H1_BSMUMUTRACKPLUSISOTWO": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH1', 'BSMUMUTRACKPLUSISOTWO', -999.)",
        "H1_ISOTWOBODYQPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH1', 'ISOTWOBODYQPLUS', -999.)",
        "H1_ISOTWOBODYMASSISOPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH1', 'ISOTWOBODYMASSISOPLUS', -999.)",
        "H1_ISOTWOBODYCHI2ISOPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH1', 'ISOTWOBODYCHI2ISOPLUS', -999.)",
        "H1_ISOTWOBODYISO5PLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH1', 'ISOTWOBODYISO5PLUS', -999.)",
        "H1_BSMUMUTRACKID": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH1', 'BSMUMUTRACKID', -999.)",
        "H1_BSMUMUTRACKTOPID": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH1', 'BSMUMUTRACKTOPID', -999.)",

        "H2_BSMUMUTRACKPLUSISO": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH2', 'BSMUMUTRACKPLUSISO', -999.)",
        "H2_BSMUMUTRACKPLUSISOTWO": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH2', 'BSMUMUTRACKPLUSISOTWO', -999.)",
        "H2_ISOTWOBODYQPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH2', 'ISOTWOBODYQPLUS', -999.)",
        "H2_ISOTWOBODYMASSISOPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH2', 'ISOTWOBODYMASSISOPLUS', -999.)",
        "H2_ISOTWOBODYCHI2ISOPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH2', 'ISOTWOBODYCHI2ISOPLUS', -999.)",
        "H2_ISOTWOBODYISO5PLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH2', 'ISOTWOBODYISO5PLUS', -999.)",
        "H2_BSMUMUTRACKID": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH2', 'BSMUMUTRACKID', -999.)",
        "H2_BSMUMUTRACKTOPID": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoH2', 'BSMUMUTRACKTOPID', -999.)",

        "L1_BSMUMUTRACKPLUSISO": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL1', 'BSMUMUTRACKPLUSISO', -999.)",
        "L1_BSMUMUTRACKPLUSISOTWO": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL1', 'BSMUMUTRACKPLUSISOTWO', -999.)",
        "L1_ISOTWOBODYQPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL1', 'ISOTWOBODYQPLUS', -999.)",
        "L1_ISOTWOBODYMASSISOPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL1', 'ISOTWOBODYMASSISOPLUS', -999.)",
        "L1_ISOTWOBODYCHI2ISOPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL1', 'ISOTWOBODYCHI2ISOPLUS', -999.)",
        "L1_ISOTWOBODYISO5PLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL1', 'ISOTWOBODYISO5PLUS', -999.)",
        "L1_BSMUMUTRACKID": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL1', 'BSMUMUTRACKID', -999.)",
        "L1_BSMUMUTRACKTOPID": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL1', 'BSMUMUTRACKTOPID', -999.)",

        "L2_BSMUMUTRACKPLUSISO": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL2', 'BSMUMUTRACKPLUSISO', -999.)",
        "L2_BSMUMUTRACKPLUSISOTWO": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL2', 'BSMUMUTRACKPLUSISOTWO', -999.)",
        "L2_ISOTWOBODYQPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL2', 'ISOTWOBODYQPLUS', -999.)",
        "L2_ISOTWOBODYMASSISOPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL2', 'ISOTWOBODYMASSISOPLUS', -999.)",
        "L2_ISOTWOBODYCHI2ISOPLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL2', 'ISOTWOBODYCHI2ISOPLUS', -999.)",
        "L2_ISOTWOBODYISO5PLUS": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL2', 'ISOTWOBODYISO5PLUS', -999.)",
        "L2_BSMUMUTRACKID": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL2', 'BSMUMUTRACKID', -999.)",
        "L2_BSMUMUTRACKTOPID": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBs2MMInfoL2', 'BSMUMUTRACKTOPID', -999.)",

        "H1_CC_ANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_ANGLE', -999.)",
        "H1_CC_MULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_MULT', -999.)",
        "H1_CC_SPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_SPT', -999.)",
        "H1_CC_VPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_VPT', -999.)",
        "H1_CC_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_PX', -999.)",
        "H1_CC_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_PY', -999.)",
        "H1_CC_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_PZ', -999.)",
        "H1_CC_PASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_PASYM', -999.)",
        "H1_CC_PTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_PTASYM', -999.)",
        "H1_CC_PXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_PXASYM', -999.)",
        "H1_CC_PYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_PYASYM', -999.)",
        "H1_CC_PZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_PZASYM', -999.)",
        "H1_CC_DELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_DELTAETA', -999.)",
        "H1_DELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_DELTAPHI', -999.)",
        "H1_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_IT', -999.)",
        "H1_MAXPT_Q": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_MAXPT_Q', -999.)",
        "H1_CC_MAXPT_PT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_MAXPT_PT', -999.)",
        "H1_CC_MAXPT_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_MAXPT_PX', -999.)",
        "H1_CC_MAXPT_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_MAXPT_PY', -999.)",
        "H1_CC_MAXPT_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_MAXPT_PZ', -999.)",
        "H1_CC_MAXPT_PE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CC_MAXPT_PE', -999.)",
        "H1_NC_ANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_ANGLE', -999.)",
        "H1_NC_MULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_MULT', -999.)",
        "H1_NC_SPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_SPT', -999.)",
        "H1_NC_VPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_VPT', -999.)",
        "H1_NC_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_PX', -999.)",
        "H1_NC_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_PY', -999.)",
        "H1_NC_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_PZ', -999.)",
        "H1_NC_PASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_PASYM', -999.)",
        "H1_NC_PTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_PTASYM', -999.)",
        "H1_NC_PXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_PXASYM', -999.)",
        "H1_NC_PYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_PYASYM', -999.)",
        "H1_NC_PZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_PZASYM', -999.)",
        "H1_NC_DELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_DELTAETA', -999.)",
        "H1_DELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_DELTAPHI', -999.)",
        "H1_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_IT', -999.)",
        "H1_NC_MAXPT_PT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_MAXPT_PT', -999.)",
        "H1_NC_MAXPT_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_MAXPT_PX', -999.)",
        "H1_NC_MAXPT_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_MAXPT_PY', -999.)",
        "H1_NC_MAXPT_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'NC_MAXPT_PZ', -999.)",
        "H1_CCNC_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH1', 'CCNC_IT', -999.)",

        "H2_CC_ANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_ANGLE', -999.)",
        "H2_CC_MULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_MULT', -999.)",
        "H2_CC_SPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_SPT', -999.)",
        "H2_CC_VPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_VPT', -999.)",
        "H2_CC_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_PX', -999.)",
        "H2_CC_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_PY', -999.)",
        "H2_CC_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_PZ', -999.)",
        "H2_CC_PASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_PASYM', -999.)",
        "H2_CC_PTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_PTASYM', -999.)",
        "H2_CC_PXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_PXASYM', -999.)",
        "H2_CC_PYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_PYASYM', -999.)",
        "H2_CC_PZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_PZASYM', -999.)",
        "H2_CC_DELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_DELTAETA', -999.)",
        "H2_DELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_DELTAPHI', -999.)",
        "H2_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_IT', -999.)",
        "H2_MAXPT_Q": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_MAXPT_Q', -999.)",
        "H2_CC_MAXPT_PT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_MAXPT_PT', -999.)",
        "H2_CC_MAXPT_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_MAXPT_PX', -999.)",
        "H2_CC_MAXPT_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_MAXPT_PY', -999.)",
        "H2_CC_MAXPT_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_MAXPT_PZ', -999.)",
        "H2_CC_MAXPT_PE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CC_MAXPT_PE', -999.)",
        "H2_NC_ANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_ANGLE', -999.)",
        "H2_NC_MULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_MULT', -999.)",
        "H2_NC_SPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_SPT', -999.)",
        "H2_NC_VPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_VPT', -999.)",
        "H2_NC_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_PX', -999.)",
        "H2_NC_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_PY', -999.)",
        "H2_NC_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_PZ', -999.)",
        "H2_NC_PASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_PASYM', -999.)",
        "H2_NC_PTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_PTASYM', -999.)",
        "H2_NC_PXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_PXASYM', -999.)",
        "H2_NC_PYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_PYASYM', -999.)",
        "H2_NC_PZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_PZASYM', -999.)",
        "H2_NC_DELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_DELTAETA', -999.)",
        "H2_DELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_DELTAPHI', -999.)",
        "H2_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_IT', -999.)",
        "H2_NC_MAXPT_PT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_MAXPT_PT', -999.)",
        "H2_NC_MAXPT_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_MAXPT_PX', -999.)",
        "H2_NC_MAXPT_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_MAXPT_PY', -999.)",
        "H2_NC_MAXPT_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'NC_MAXPT_PZ', -999.)",
        "H2_CCNC_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoH2', 'CCNC_IT', -999.)",

        "L1_CC_ANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_ANGLE', -999.)",
        "L1_CC_MULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_MULT', -999.)",
        "L1_CC_SPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_SPT', -999.)",
        "L1_CC_VPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_VPT', -999.)",
        "L1_CC_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_PX', -999.)",
        "L1_CC_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_PY', -999.)",
        "L1_CC_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_PZ', -999.)",
        "L1_CC_PASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_PASYM', -999.)",
        "L1_CC_PTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_PTASYM', -999.)",
        "L1_CC_PXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_PXASYM', -999.)",
        "L1_CC_PYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_PYASYM', -999.)",
        "L1_CC_PZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_PZASYM', -999.)",
        "L1_CC_DELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_DELTAETA', -999.)",
        "L1_DELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_DELTAPHI', -999.)",
        "L1_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_IT', -999.)",
        "L1_MAXPT_Q": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_MAXPT_Q', -999.)",
        "L1_CC_MAXPT_PT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_MAXPT_PT', -999.)",
        "L1_CC_MAXPT_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_MAXPT_PX', -999.)",
        "L1_CC_MAXPT_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_MAXPT_PY', -999.)",
        "L1_CC_MAXPT_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_MAXPT_PZ', -999.)",
        "L1_CC_MAXPT_PE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CC_MAXPT_PE', -999.)",
        "L1_NC_ANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_ANGLE', -999.)",
        "L1_NC_MULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_MULT', -999.)",
        "L1_NC_SPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_SPT', -999.)",
        "L1_NC_VPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_VPT', -999.)",
        "L1_NC_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_PX', -999.)",
        "L1_NC_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_PY', -999.)",
        "L1_NC_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_PZ', -999.)",
        "L1_NC_PASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_PASYM', -999.)",
        "L1_NC_PTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_PTASYM', -999.)",
        "L1_NC_PXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_PXASYM', -999.)",
        "L1_NC_PYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_PYASYM', -999.)",
        "L1_NC_PZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_PZASYM', -999.)",
        "L1_NC_DELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_DELTAETA', -999.)",
        "L1_DELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_DELTAPHI', -999.)",
        "L1_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_IT', -999.)",
        "L1_NC_MAXPT_PT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_MAXPT_PT', -999.)",
        "L1_NC_MAXPT_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_MAXPT_PX', -999.)",
        "L1_NC_MAXPT_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_MAXPT_PY', -999.)",
        "L1_NC_MAXPT_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'NC_MAXPT_PZ', -999.)",
        "L1_CCNC_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL1', 'CCNC_IT', -999.)",

        "L2_CC_ANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_ANGLE', -999.)",
        "L2_CC_MULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_MULT', -999.)",
        "L2_CC_SPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_SPT', -999.)",
        "L2_CC_VPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_VPT', -999.)",
        "L2_CC_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_PX', -999.)",
        "L2_CC_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_PY', -999.)",
        "L2_CC_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_PZ', -999.)",
        "L2_CC_PASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_PASYM', -999.)",
        "L2_CC_PTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_PTASYM', -999.)",
        "L2_CC_PXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_PXASYM', -999.)",
        "L2_CC_PYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_PYASYM', -999.)",
        "L2_CC_PZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_PZASYM', -999.)",
        "L2_CC_DELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_DELTAETA', -999.)",
        "L2_DELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_DELTAPHI', -999.)",
        "L2_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_IT', -999.)",
        "L2_MAXPT_Q": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_MAXPT_Q', -999.)",
        "L2_CC_MAXPT_PT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_MAXPT_PT', -999.)",
        "L2_CC_MAXPT_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_MAXPT_PX', -999.)",
        "L2_CC_MAXPT_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_MAXPT_PY', -999.)",
        "L2_CC_MAXPT_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_MAXPT_PZ', -999.)",
        "L2_CC_MAXPT_PE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CC_MAXPT_PE', -999.)",
        "L2_NC_ANGLE": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_ANGLE', -999.)",
        "L2_NC_MULT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_MULT', -999.)",
        "L2_NC_SPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_SPT', -999.)",
        "L2_NC_VPT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_VPT', -999.)",
        "L2_NC_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_PX', -999.)",
        "L2_NC_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_PY', -999.)",
        "L2_NC_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_PZ', -999.)",
        "L2_NC_PASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_PASYM', -999.)",
        "L2_NC_PTASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_PTASYM', -999.)",
        "L2_NC_PXASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_PXASYM', -999.)",
        "L2_NC_PYASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_PYASYM', -999.)",
        "L2_NC_PZASYM": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_PZASYM', -999.)",
        "L2_NC_DELTAETA": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_DELTAETA', -999.)",
        "L2_DELTAPHI": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_DELTAPHI', -999.)",
        "L2_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_IT', -999.)",
        "L2_NC_MAXPT_PT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_MAXPT_PT', -999.)",
        "L2_NC_MAXPT_PX": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_MAXPT_PX', -999.)",
        "L2_NC_MAXPT_PY": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_MAXPT_PY', -999.)",
        "L2_NC_MAXPT_PZ": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'NC_MAXPT_PZ', -999.)",
        "L2_CCNC_IT": "RELINFO('Phys/Bu2LLK_eeLine2_extra/ConeIsoInfoL2', 'CCNC_IT', -999.)",

        "H1_TRKISOBDT1": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoH1', 'TRKISOBDTFIRSTVALUE', -999.)",
        "H1_TRKISOBDT2": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoH1', 'TRKISOBDTSECONDVALUE', -999.)",
        "H1_TRKISOBDT3": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoH1', 'TRKISOBDTTHIRDVALUE ', -999.)",

        "H2_TRKISOBDT1": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoH2', 'TRKISOBDTFIRSTVALUE', -999.)",
        "H2_TRKISOBDT2": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoH2', 'TRKISOBDTSECONDVALUE', -999.)",
        "H2_TRKISOBDT3": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoH2', 'TRKISOBDTTHIRDVALUE ', -999.)",

        "L1_TRKISOBDT1": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoL1', 'TRKISOBDTFIRSTVALUE', -999.)",
        "L1_TRKISOBDT2": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoL1', 'TRKISOBDTSECONDVALUE', -999.)",
        "L1_TRKISOBDT3": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoL1', 'TRKISOBDTTHIRDVALUE ', -999.)",

        "L2_TRKISOBDT1": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoL2', 'TRKISOBDTFIRSTVALUE', -999.)",
        "L2_TRKISOBDT2": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoL2', 'TRKISOBDTSECONDVALUE', -999.)",
        "L2_TRKISOBDT3": "RELINFO('Phys/Bu2LLK_eeLine2_extra/TrackIsoBDTInfoL2', 'TRKISOBDTTHIRDVALUE ', -999.)"
    }
  
    ntuple.B.addTool(LoKi_Extra)
    ntuple.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Extra"]

    LoKi_DTF = LoKi__Hybrid__TupleTool('LoKi_DTF')
    LoKi_DTF.Variables = {
        ###############################################################################
        #                                                                             #
        # First set of DTF variables (identified by PVandJpsiDTF in the branch name). #
        #                                                                             #
        ###############################################################################
        # B variables
        "PVandJpsiDTF_B_M"        : "DTF_FUN(M,True,'J/psi(1S)')",
        "PVandJpsiDTF_B_CHI2NDOF" : "DTF_CHI2NDOF(True,'J/psi(1S)')",
        "PVandJpsiDTF_B_CHI2"     : "DTF_CHI2(True,'J/psi(1S)')",
        "PVandJpsiDTF_B_NDOF"     : "DTF_NDOF(True,'J/psi(1S)')",
        "PVandJpsiDTF_B_P"        : "DTF_FUN(P,True,'J/psi(1S)')",
        "PVandJpsiDTF_B_PT"       : "DTF_FUN(PT,True,'J/psi(1S)')",
	"PVandJpsiDTF_B_PX"       : "DTF_FUN(PX,True,'J/psi(1S)')",
	"PVandJpsiDTF_B_PY"       : "DTF_FUN(PY,True,'J/psi(1S)')",
	"PVandJpsiDTF_B_PZ"       : "DTF_FUN(PZ,True,'J/psi(1S)')",
	"PVandJpsiDTF_B_PHI"	 : "DTF_FUN(PHI,True,'J/psi(1S)')",
	"PVandJpsiDTF_B_ETA"	 : "DTF_FUN(ETA,True,'J/psi(1S)')",
	"PVandJpsiDTF_B_THETA"	 : "DTF_FUN(atan(PT/PZ),True,'J/psi(1S)')",
	# Jpsi variables
        "PVandJpsiDTF_Jpsi_M"     : "DTF_FUN(CHILD(1,M),True,'J/psi(1S)')",
        "PVandJpsiDTF_Jpsi_P"     : "DTF_FUN(CHILD(1,P),True,'J/psi(1S)')",
        "PVandJpsiDTF_Jpsi_PT"    : "DTF_FUN(CHILD(1,PT),True,'J/psi(1S)')",
	"PVandJpsiDTF_Jpsi_PX"    : "DTF_FUN(CHILD(1,PX),True,'J/psi(1S)')",
	"PVandJpsiDTF_Jpsi_PY"    : "DTF_FUN(CHILD(1,PY),True,'J/psi(1S)')",
	"PVandJpsiDTF_Jpsi_PZ"    : "DTF_FUN(CHILD(1,PZ),True,'J/psi(1S)')",
	"PVandJpsiDTF_Jpsi_PHI"   : "DTF_FUN(CHILD(1,PHI),True,'J/psi(1S)')",
	"PVandJpsiDTF_Jpsi_ETA"   : "DTF_FUN(CHILD(1,ETA),True,'J/psi(1S)')",
	"PVandJpsiDTF_Jpsi_THETA" : "DTF_FUN(CHILD(1,atan(PT/PZ)),True,'J/psi(1S)')",
	# Kstar variables
        "PVandJpsiDTF_Kstar_M"    : "DTF_FUN(CHILD(2,M),True,'J/psi(1S)')",
        "PVandJpsiDTF_Kstar_P"    : "DTF_FUN(CHILD(2,P),True,'J/psi(1S)')",
        "PVandJpsiDTF_Kstar_PT"   : "DTF_FUN(CHILD(2,PT),True,'J/psi(1S)')",
	"PVandJpsiDTF_Kstar_PX"   : "DTF_FUN(CHILD(2,PX),True,'J/psi(1S)')",
	"PVandJpsiDTF_Kstar_PY"   : "DTF_FUN(CHILD(2,PY),True,'J/psi(1S)')",
	"PVandJpsiDTF_Kstar_PZ"   : "DTF_FUN(CHILD(2,PZ),True,'J/psi(1S)')",
	"PVandJpsiDTF_Kstar_PHI"  : "DTF_FUN(CHILD(2,PHI),True,'J/psi(1S)')",
	"PVandJpsiDTF_Kstar_ETA"  : "DTF_FUN(CHILD(2,ETA),True,'J/psi(1S)')",
	"PVandJpsiDTF_Kstar_THETA": "DTF_FUN(CHILD(2,atan(PT/PZ)),True,'J/psi(1S)')",
	# L1 variables
        "PVandJpsiDTF_L1_M"     : "DTF_FUN(CHILD(1,CHILD(1,M)),True,'J/psi(1S)')",
        "PVandJpsiDTF_L1_P"     : "DTF_FUN(CHILD(1,CHILD(1,P)),True,'J/psi(1S)')",
        "PVandJpsiDTF_L1_PT"    : "DTF_FUN(CHILD(1,CHILD(1,PT)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L1_PX"    : "DTF_FUN(CHILD(1,CHILD(1,PX)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L1_PY"    : "DTF_FUN(CHILD(1,CHILD(1,PY)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L1_PZ"    : "DTF_FUN(CHILD(1,CHILD(1,PZ)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L1_PHI"   : "DTF_FUN(CHILD(1,CHILD(1,PHI)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L1_ETA"   : "DTF_FUN(CHILD(1,CHILD(1,ETA)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L1_THETA" : "DTF_FUN(CHILD(1,CHILD(1,atan(PT/PZ))),True,'J/psi(1S)')",
	# L2 variables
        "PVandJpsiDTF_L2_M"     : "DTF_FUN(CHILD(1,CHILD(2,M)),True,'J/psi(1S)')",
        "PVandJpsiDTF_L2_P"     : "DTF_FUN(CHILD(1,CHILD(2,P)),True,'J/psi(1S)')",
        "PVandJpsiDTF_L2_PT"    : "DTF_FUN(CHILD(1,CHILD(2,PT)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L2_PX"    : "DTF_FUN(CHILD(1,CHILD(2,PX)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L2_PY"    : "DTF_FUN(CHILD(1,CHILD(2,PY)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L2_PZ"    : "DTF_FUN(CHILD(1,CHILD(2,PZ)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L2_PHI"   : "DTF_FUN(CHILD(1,CHILD(2,PHI)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L2_ETA"   : "DTF_FUN(CHILD(1,CHILD(2,ETA)),True,'J/psi(1S)')",
	"PVandJpsiDTF_L2_THETA" : "DTF_FUN(CHILD(1,CHILD(2,atan(PT/PZ))),True,'J/psi(1S)')",
	# K variables
        "PVandJpsiDTF_K_M"     : "DTF_FUN(CHILD(2,CHILD(1,M)),True,'J/psi(1S)')",
        "PVandJpsiDTF_K_P"     : "DTF_FUN(CHILD(2,CHILD(1,P)),True,'J/psi(1S)')",
        "PVandJpsiDTF_K_PT"    : "DTF_FUN(CHILD(2,CHILD(1,PT)),True,'J/psi(1S)')",
	"PVandJpsiDTF_K_PX"    : "DTF_FUN(CHILD(2,CHILD(1,PX)),True,'J/psi(1S)')",
	"PVandJpsiDTF_K_PY"    : "DTF_FUN(CHILD(2,CHILD(1,PY)),True,'J/psi(1S)')",
	"PVandJpsiDTF_K_PZ"    : "DTF_FUN(CHILD(2,CHILD(1,PZ)),True,'J/psi(1S)')",
	"PVandJpsiDTF_K_PHI"   : "DTF_FUN(CHILD(2,CHILD(1,PHI)),True,'J/psi(1S)')",
	"PVandJpsiDTF_K_ETA"   : "DTF_FUN(CHILD(2,CHILD(1,ETA)),True,'J/psi(1S)')",
	"PVandJpsiDTF_K_THETA" : "DTF_FUN(CHILD(2,CHILD(1,atan(PT/PZ))),True,'J/psi(1S)')",
	# Pi variables
        "PVandJpsiDTF_Pi_M"     : "DTF_FUN(CHILD(2,CHILD(2,M)),True,'J/psi(1S)')",
        "PVandJpsiDTF_Pi_P"     : "DTF_FUN(CHILD(2,CHILD(2,P)),True,'J/psi(1S)')",
        "PVandJpsiDTF_Pi_PT"    : "DTF_FUN(CHILD(2,CHILD(2,PT)),True,'J/psi(1S)')",
	"PVandJpsiDTF_Pi_PX"    : "DTF_FUN(CHILD(2,CHILD(2,PX)),True,'J/psi(1S)')",
	"PVandJpsiDTF_Pi_PY"    : "DTF_FUN(CHILD(2,CHILD(2,PY)),True,'J/psi(1S)')",
	"PVandJpsiDTF_Pi_PZ"    : "DTF_FUN(CHILD(2,CHILD(2,PZ)),True,'J/psi(1S)')",
	"PVandJpsiDTF_Pi_PHI"   : "DTF_FUN(CHILD(2,CHILD(2,PHI)),True,'J/psi(1S)')",
	"PVandJpsiDTF_Pi_ETA"   : "DTF_FUN(CHILD(2,CHILD(2,ETA)),True,'J/psi(1S)')",
	"PVandJpsiDTF_Pi_THETA" : "DTF_FUN(CHILD(2,CHILD(2,atan(PT/PZ))),True,'J/psi(1S)')",
        #############################################################################
        #                                                                           #
        # Second set of DTF variables (identified by PVandBDTF in the branch name). #
        #                                                                           #
        #############################################################################
	# B variables
        "PVandBDTF_B_M"        : "DTF_FUN(M,True,'B0')",
        "PVandBDTF_B_CHI2NDOF" : "DTF_CHI2NDOF(True,'B0')",
        "PVandBDTF_B_CHI2"     : "DTF_CHI2(True,'B0')",
        "PVandBDTF_B_NDOF"     : "DTF_NDOF(True,'B0')",
        "PVandBDTF_B_P"        : "DTF_FUN(P,True,'B0')",
        "PVandBDTF_B_PT"       : "DTF_FUN(PT,True,'B0')",
	"PVandBDTF_B_PX"       : "DTF_FUN(PX,True,'B0')",
	"PVandBDTF_B_PY"       : "DTF_FUN(PY,True,'B0')",
	"PVandBDTF_B_PZ"       : "DTF_FUN(PZ,True,'B0')",
	"PVandBDTF_B_PHI"      : "DTF_FUN(PHI,True,'B0')",
	"PVandBDTF_B_ETA"      : "DTF_FUN(ETA,True,'B0')",
	"PVandBDTF_B_THETA"    : "DTF_FUN(atan(PT/PZ),True,'B0')",
	# Jpsi variables
        "PVandBDTF_Jpsi_M"     : "DTF_FUN(CHILD(1,M),True,'B0')",
        "PVandBDTF_Jpsi_P"     : "DTF_FUN(CHILD(1,P),True,'B0')",
        "PVandBDTF_Jpsi_PT"    : "DTF_FUN(CHILD(1,PT),True,'B0')",
	"PVandBDTF_Jpsi_PX"    : "DTF_FUN(CHILD(1,PX),True,'B0')",
	"PVandBDTF_Jpsi_PY"    : "DTF_FUN(CHILD(1,PY),True,'B0')",
	"PVandBDTF_Jpsi_PZ"    : "DTF_FUN(CHILD(1,PZ),True,'B0')",
	"PVandBDTF_Jpsi_PHI"   : "DTF_FUN(CHILD(1,PHI),True,'B0')",
	"PVandBDTF_Jpsi_ETA"   : "DTF_FUN(CHILD(1,ETA),True,'B0')",
	"PVandBDTF_Jpsi_THETA" : "DTF_FUN(CHILD(1,atan(PT/PZ)),True,'B0')",
	# Kstar variables
        "PVandBDTF_Kstar_M"    : "DTF_FUN(CHILD(2,M),True,'B0')",
        "PVandBDTF_Kstar_P"    : "DTF_FUN(CHILD(2,P),True,'B0')",
        "PVandBDTF_Kstar_PT"   : "DTF_FUN(CHILD(2,PT),True,'B0')",
	"PVandBDTF_Kstar_PX"   : "DTF_FUN(CHILD(2,PX),True,'B0')",
	"PVandBDTF_Kstar_PY"   : "DTF_FUN(CHILD(2,PY),True,'B0')",
	"PVandBDTF_Kstar_PZ"   : "DTF_FUN(CHILD(2,PZ),True,'B0')",
	"PVandBDTF_Kstar_PHI"  : "DTF_FUN(CHILD(2,PHI),True,'B0')",
	"PVandBDTF_Kstar_ETA"  : "DTF_FUN(CHILD(2,ETA),True,'B0')",
	"PVandBDTF_Kstar_THETA": "DTF_FUN(CHILD(2,atan(PT/PZ)),True,'B0')",
	# L1 variables
        "PVandBDTF_L1_M"     : "DTF_FUN(CHILD(1,CHILD(1,M)),True,'B0')",
        "PVandBDTF_L1_P"     : "DTF_FUN(CHILD(1,CHILD(1,P)),True,'B0')",
        "PVandBDTF_L1_PT"    : "DTF_FUN(CHILD(1,CHILD(1,PT)),True,'B0')",
	"PVandBDTF_L1_PX"    : "DTF_FUN(CHILD(1,CHILD(1,PX)),True,'B0')",
	"PVandBDTF_L1_PY"    : "DTF_FUN(CHILD(1,CHILD(1,PY)),True,'B0')",
	"PVandBDTF_L1_PZ"    : "DTF_FUN(CHILD(1,CHILD(1,PZ)),True,'B0')",
	"PVandBDTF_L1_PHI"   : "DTF_FUN(CHILD(1,CHILD(1,PHI)),True,'B0')",
	"PVandBDTF_L1_ETA"   : "DTF_FUN(CHILD(1,CHILD(1,ETA)),True,'B0')",
	"PVandBDTF_L1_THETA" : "DTF_FUN(CHILD(1,CHILD(1,atan(PT/PZ))),True,'B0')",
	# L2 variables
        "PVandBDTF_L2_M"     : "DTF_FUN(CHILD(1,CHILD(2,M)),True,'B0')",
        "PVandBDTF_L2_P"     : "DTF_FUN(CHILD(1,CHILD(2,P)),True,'B0')",
        "PVandBDTF_L2_PT"    : "DTF_FUN(CHILD(1,CHILD(2,PT)),True,'B0')",
	"PVandBDTF_L2_PX"    : "DTF_FUN(CHILD(1,CHILD(2,PX)),True,'B0')",
	"PVandBDTF_L2_PY"    : "DTF_FUN(CHILD(1,CHILD(2,PY)),True,'B0')",
	"PVandBDTF_L2_PZ"    : "DTF_FUN(CHILD(1,CHILD(2,PZ)),True,'B0')",
	"PVandBDTF_L2_PHI"   : "DTF_FUN(CHILD(1,CHILD(2,PHI)),True,'B0')",
	"PVandBDTF_L2_ETA"   : "DTF_FUN(CHILD(1,CHILD(2,ETA)),True,'B0')",
	"PVandBDTF_L2_THETA" : "DTF_FUN(CHILD(1,CHILD(2,atan(PT/PZ))),True,'B0')",
	# K variables
        "PVandBDTF_K_M"     : "DTF_FUN(CHILD(2,CHILD(1,M)),True,'B0')",
        "PVandBDTF_K_P"     : "DTF_FUN(CHILD(2,CHILD(1,P)),True,'B0')",
        "PVandBDTF_K_PT"    : "DTF_FUN(CHILD(2,CHILD(1,PT)),True,'B0')",
	"PVandBDTF_K_PX"    : "DTF_FUN(CHILD(2,CHILD(1,PX)),True,'B0')",
	"PVandBDTF_K_PY"    : "DTF_FUN(CHILD(2,CHILD(1,PY)),True,'B0')",
	"PVandBDTF_K_PZ"    : "DTF_FUN(CHILD(2,CHILD(1,PZ)),True,'B0')",
	"PVandBDTF_K_PHI"   : "DTF_FUN(CHILD(2,CHILD(1,PHI)),True,'B0')",
	"PVandBDTF_K_ETA"   : "DTF_FUN(CHILD(2,CHILD(1,ETA)),True,'B0')",
	"PVandBDTF_K_THETA" : "DTF_FUN(CHILD(2,CHILD(1,atan(PT/PZ))),True,'B0')",
	# Pi variables
        "PVandBDTF_Pi_M"     : "DTF_FUN(CHILD(2,CHILD(2,M)),True,'B0')",
        "PVandBDTF_Pi_P"     : "DTF_FUN(CHILD(2,CHILD(2,P)),True,'B0')",
        "PVandBDTF_Pi_PT"    : "DTF_FUN(CHILD(2,CHILD(2,PT)),True,'B0')",
	"PVandBDTF_Pi_PX"    : "DTF_FUN(CHILD(2,CHILD(2,PX)),True,'B0')",
	"PVandBDTF_Pi_PY"    : "DTF_FUN(CHILD(2,CHILD(2,PY)),True,'B0')",
	"PVandBDTF_Pi_PZ"    : "DTF_FUN(CHILD(2,CHILD(2,PZ)),True,'B0')",
	"PVandBDTF_Pi_PHI"   : "DTF_FUN(CHILD(2,CHILD(2,PHI)),True,'B0')",
	"PVandBDTF_Pi_ETA"   : "DTF_FUN(CHILD(2,CHILD(2,ETA)),True,'B0')",
	"PVandBDTF_Pi_THETA" : "DTF_FUN(CHILD(2,CHILD(2,atan(PT/PZ))),True,'B0')",
    }
    ntuple.B.addTool(LoKi_DTF)
    ntuple.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_DTF"]

    LoKi_angular = ntuple.addTupleTool("LoKi::Hybrid::TupleTool")
    LoKi_angular.Variables = {
        "eta": "ETA",
        "phi": "PHI",
    }

    LoKi_COV = LoKi__Hybrid__TupleTool("LoKi_COV")
    LoKi_COV.Preambulo = [
        "from LoKiTracks.decorators import *"
    ]
    LoKi_COV.Variables = {
        "TrPX":    "TRFUN(TrPX)",
        "TrPY":    "TRFUN(TrPY)",
        "TrPZ":    "TRFUN(TrPZ)",
        "TrP":     "TRFUN(TrP)",
        "TrQ":     "TRFUN(TrQ)",
        "PCOV00":  "PCOV2(0,0)",
        "PCOV10":  "PCOV2(1,0)",
        "PCOV20":  "PCOV2(2,0)",
        "PCOV30":  "PCOV2(3,0)",
        "PCOV01":  "PCOV2(0,1)",
        "PCOV11":  "PCOV2(1,1)",
        "PCOV21":  "PCOV2(2,1)",
        "PCOV31":  "PCOV2(3,1)",
        "PCOV02":  "PCOV2(0,2)",
        "PCOV12":  "PCOV2(1,2)",
        "PCOV22":  "PCOV2(2,2)",
        "PCOV32":  "PCOV2(3,2)",
        "PCOV03":  "PCOV2(0,3)",
        "PCOV13":  "PCOV2(1,3)",
        "PCOV23":  "PCOV2(2,3)",
        "PCOV33":  "PCOV2(3,3)",
        "TrCOV00": "TRFUN(TrCOV2(0,0))",
        "TrCOV01": "TRFUN(TrCOV2(0,1))",
        "TrCOV02": "TRFUN(TrCOV2(0,2))",
        "TrCOV03": "TRFUN(TrCOV2(0,3))",
        "TrCOV04": "TRFUN(TrCOV2(0,4))",
        "TrCOV10": "TRFUN(TrCOV2(1,0))",
        "TrCOV11": "TRFUN(TrCOV2(1,1))",
        "TrCOV12": "TRFUN(TrCOV2(1,2))",
        "TrCOV13": "TRFUN(TrCOV2(1,3))",
        "TrCOV14": "TRFUN(TrCOV2(1,4))",
        "TrCOV20": "TRFUN(TrCOV2(2,0))",
        "TrCOV21": "TRFUN(TrCOV2(2,1))",
        "TrCOV22": "TRFUN(TrCOV2(2,2))",
        "TrCOV23": "TRFUN(TrCOV2(2,3))",
        "TrCOV24": "TRFUN(TrCOV2(2,4))",
        "TrCOV30": "TRFUN(TrCOV2(3,0))",
        "TrCOV31": "TRFUN(TrCOV2(3,1))",
        "TrCOV32": "TRFUN(TrCOV2(3,2))",
        "TrCOV33": "TRFUN(TrCOV2(3,3))",
        "TrCOV34": "TRFUN(TrCOV2(3,4))",
        "TrCOV40": "TRFUN(TrCOV2(4,0))",
        "TrCOV41": "TRFUN(TrCOV2(4,1))",
        "TrCOV42": "TRFUN(TrCOV2(4,2))",
        "TrCOV43": "TRFUN(TrCOV2(4,3))",
        "TrCOV44": "TRFUN(TrCOV2(4,4))",
    }

    def add_COV(track):
        track.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_COV"]
        track.addTool(LoKi_COV)

    add_COV(ntuple.K)
    add_COV(ntuple.Pi)
    add_COV(ntuple.L1)
    add_COV(ntuple.L2)

    ################################
    ###   DaVinci configuration ####
    ################################

    DaVinci().UserAlgorithms += [sequencer[channel]]

    DaVinci().EvtMax = -1
    DaVinci().SkipEvents = 0
    if DaVinci().Simulation is False:
        DaVinci().RootInTES = "/Event/Leptonic/"
        DaVinci().Lumi = True

