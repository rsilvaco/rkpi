def merge_options(optsfile, extraopts):
   with open('the_opts.py', 'w') as o, open(optsfile, 'r') as f:
      for line in f:
         o.write(line)
      o.write(extraopts)

jobsDicts = [
    # 2011
    {
    "name" : "B02Kpimumu-Data-2011-MagDown",
    "inputdata" : "/LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1p2/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2011",
    "DDDB" : "dddb-20190206-1",
    },
    {
    "name" : "B02Kpimumu-Data-2011-MagUp",
    "inputdata" : "/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r1p2/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2011",
    "DDDB" : "dddb-20190206-1",
    },
    # 2012
    {
    "name" : "B02Kpimumu-Data-2012-MagDown",
    "inputdata" : "/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r0p2/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2012",
    "DDDB" : "dddb-20190206-2",
    },
    {
    "name" : "B02Kpimumu-Data-2012-MagUp",
    "inputdata" : "/LHCb/Collision12/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r0p2/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2012",
    "DDDB" : "dddb-20190206-2",
    },
    # 2015
    {
    "name" : "B02Kpimumu-Data-2015-MagDown",
    "inputdata" : "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2015",
    "DDDB" : "dddb-20190206-3",
    },
    {
    "name" : "B02Kpimumu-Data-2015-MagUp",
    "inputdata" : "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2015",
    "DDDB" : "dddb-20190206-3",
    },
    # 2016
    {
    "name" : "B02Kpimumu-Data-2016-MagDown",
    "inputdata" : "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2016",
    "DDDB" : "dddb-20190206-3",
    },
    {
    "name" : "B02Kpimumu-Data-2016-MagUp",
    "inputdata" : "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2016",
    "DDDB" : "dddb-20190206-3",
    },
    # 2017
    {
    "name" : "B02Kpimumu-Data-2017-MagDown",
    "inputdata" : "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2p1/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2017",
    "DDDB" : "dddb-20190206-3"
    },
    {
    "name" : "B02Kpimumu-Data-2017-MagUp",
    "inputdata" : "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2p1/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2017",
    "DDDB" : "dddb-20190206-3"
    },
    # 2018
    {
    "name" : "B02Kpimumu-Data-2018-MagDown",
    "inputdata" : "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34r0p1/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2018",
    "DDDB" : "dddb-20190206-3"
    },
    {
    "name" : "B02Kpimumu-Data-2018-MagUp",
    "inputdata" : "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34r0p1/90000000/LEPTONIC.MDST",
    "isSimulation" : "False",
    "year" : "2018",
    "DDDB" : "dddb-20190206-3"
    },
]

for jobsDict in jobsDicts:

    # If running for the first time the env, uncomment the next three lines and comment out the corresponding directory ones
    #j = Job(name=jobsDict["name"])
    #myApp = prepareGaudiExec('DaVinci','v39r1p6', myPath='.')
    #myApp = prepareGaudiExec('DaVinci','v44r0p5', myPath='.')
    #j.application = myApp

    myApp = GaudiExec()
    j = Job(application=myApp)

    if '2011' in jobsDict["year"]: 
        myApp.directory = "./DaVinciDev_v39r1p6" 
        j.application.platform = 'x86_64-slc6-gcc49-opt'
    elif '2012' in jobsDict["year"]:
        myApp.directory = "./DaVinciDev_v39r1p6" 
        j.application.platform = 'x86_64-slc6-gcc49-opt'
    elif '2015' in jobsDict["year"]:
        myApp.directory = "./DaVinciDev_v44r10p5" 
        j.application.platform = 'x86_64-centos7-gcc7-opt'
    elif '2016' in jobsDict["year"]:
        myApp.directory = "./DaVinciDev_v44r10p5" 
        j.application.platform = 'x86_64-centos7-gcc7-opt'
    elif '2017' in jobsDict["year"]:
        myApp.directory = "./DaVinciDev_v42r9p2" 
        j.application.platform = 'x86_64-centos7-gcc62-opt'
    else:
        myApp.directory = "./DaVinciDev_v44r10p2" 
        j.application.platform = 'x86_64-centos7-gcc7-opt'

    j.name = jobsDict["name"]

    limit_input_files = -1
    ds = BKQuery(jobsDict["inputdata"], dqflag=['OK']).getDataset()

    if limit_input_files > 0:
      ds_reduced = LHCbDataset()
      n = 0
      for fn in ds.getFileNames():
        if n >= limit_input_files: continue
        ds_reduced.extend( [ 'LFN:'+fn, ] )
        n = n + 1
      ds = ds_reduced  

    j.inputdata     = ds

    if jobsDict["isSimulation"] == "False":
        extraopts = """
from Configurables import CondDB
DaVinci().DataType = '{year}'
DaVinci().TupleFile  = '{name}.root'
DaVinci().Simulation = {isSimulation}
CondDB().UseLatestTags = ['{year}']
DaVinci().DDDBtag = '{DDDB}'
execute_option_file('{inputdata}')
        """.format(**jobsDict)

    #Simulation
    if jobsDict["isSimulation"] == "True":
        extraopts = """
DaVinci().DataType = '{year}'
DaVinci().TupleFile  = '{name}.root'
DaVinci().Simulation = {isSimulation}
DaVinci().CondDBtag = '{CondDB}'
DaVinci().DDDBtag = '{DDDB}'
execute_option_file('{inputdata}')
        """.format(**jobsDict)

    merge_options("options_B02KpiMM.py", extraopts)
    j.application.options = ["the_opts.py"]

    if jobsDict["isSimulation"] == "False" :
        j.splitter = SplitByFiles(filesPerJob = 5)
    else : 
        j.splitter = SplitByFiles(filesPerJob = 10)

    j.outputfiles = [DiracFile(jobsDict["name"]+".root")]

    j.inputfiles = [ "/home/hep/rsilvaco/Analysis/RareDecays/Bu2K1EE/Repository/ewp-rkpipi/weightfiles/weightsHard.xml", 
                     "/home/hep/rsilvaco/Analysis/RareDecays/Bu2K1EE/Repository/ewp-rkpipi/weightfiles/weightsSoft.xml" ]

    j.comment = jobsDict["name"]

    j.backend = Dirac()

    j.submit()
